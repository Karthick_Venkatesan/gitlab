import { initIterationsList } from 'ee/iterations';

document.addEventListener('DOMContentLoaded', () => {
  initIterationsList();
});
